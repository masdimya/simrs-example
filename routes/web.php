<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DefaultController@index')->name('dashboard');
Route::get('/page', 'DefaultController@simplePage')->name('page');

Route::name('page1.')->group(function () {
    Route::prefix('page1')->group(function () {
        Route::get('sub1', function () {
            return view('pages.page');
        })->name('sub1');
        Route::get('sub2', function () {
            return view('pages.page');
        })->name('sub2');
    });
});

Route::name('poli-anak.')->group(function () {
    Route::prefix('poli-anak')->group(function () {
        Route::get('/', function () {
            return view('pages.poli.dashboard-poli');
        })->name('dashboard');
        Route::get('list-antrian', function () {
            return view('pages.poli.daftar-antrian');
        })->name('antrian');
        Route::get('list-pasien', function () {
            return view('pages.poli.daftar-pasien');
        })->name('pasien');
        Route::get('diagnosa', function () {
            return view('pages.poli.tambah-diagnosa');
        })->name('diagnosa');
    });
});


Route::get('/informasi-pasien', function () {
    return view('pages.pasien.informasi-pasien');
})->name('informasi.pasien');
Route::name('kasir.')->group(function () {
    Route::prefix('kasir')->group(function () {
        Route::get('/', function () {
            return view('pages.kasir.dashboard');
        })->name('dashboard');
        Route::get('list-pasien', function () {
            return view('pages.kasir.daftar-pasien');
        })->name('pasien');
        Route::get('checkout', function () {
            return view('pages.kasir.detail-pembayaran');
        })->name('checkout');
    });
});

Route::get('/registrasi-pasien', function () {
    return view('pages.pasien.registrasi-pasien');
})->name('registrasi.pasien');

Route::get('/cetak-kartu-rm', function () {
    $filename = 'CetakRM-773018.pdf';
    $filePath = public_path() . "/files" . "/" . $filename;
    return response()->file($filePath); // digunakan untuk preview sebelum download
})->name('cetak.pasien');
