<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class DefaultController extends Controller
{
    public function index(Request $request){
        return view('pages.dashboard');
    }
    public function simplePage(Request $request){
        
        
       $request->session()->flash('message',['type'=>'success','body'=>'Berhasil menambahkan data 🙂']);
        
        return view('pages.page');

    }
}
