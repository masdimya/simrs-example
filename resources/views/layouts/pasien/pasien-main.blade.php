<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>@yield('title')</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{-- Main sytle insert here --}}
    @include('includes.pasien.mainstyle-pasien')

    {{-- Custom sytle insert here --}}
    {{-- @yield('custom-style') --}}


</head>

<body>
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

    <div class="wrapper">
        @include('includes.pasien.header-pasien')

        <div class="page-wrap">
            @include('includes.pasien.sidebar-pasien')

            @yield('main-content')


            @include('includes.pasien.right-sidebar-pasien')


            @include('includes.pasien.footer-pasien')


        </div>
    </div>






    {{-- Main script insert here --}}
    @include('includes.pasien.mainscript-pasien')

    {{-- Custom script insert here --}}


</body>

</html>