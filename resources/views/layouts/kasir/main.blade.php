<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>@yield('title')</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{-- Main sytle insert here --}}
    @include('includes.default.mainstyle')

    {{-- Custom sytle insert here --}}
    {{-- @yield('custom-style') --}}


</head>

<body>
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

    <div class="wrapper">
        @include('includes.default.header')

        <div class="page-wrap">
            @include('includes.kasir.sidebar')

            @yield('main-content')


            @include('includes.default.right-sidebar')


            @include('includes.default.footer')


        </div>
    </div>






    {{-- Main script insert here --}}
    @include('includes.default.mainscript')

    {{-- Custom script insert here --}}


</body>

</html>