<div class="app-sidebar colored">
    <div class="sidebar-header">
        <a class="header-brand" href="index.html">
            <div class="logo-img">
               <img src="src/img/brand-white.svg" class="header-brand-img" alt="lavalite"> 
            </div>
            <span class="text">ThemeKit</span>
        </a>
        <button type="button" class="nav-toggle"><i data-toggle="expanded" class="ik ik-toggle-right toggle-icon"></i></button>
        <button id="sidebarClose" class="nav-close"><i class="ik ik-x"></i></button>
    </div>
    
    <div class="sidebar-content">
        <div class="nav-container">
            <nav id="main-menu-navigation" class="navigation-main">
        
                <div class="nav-item {{set_active('kasir.dashboard')}}">
                    <a href="{{route('kasir.dashboard')}}"><i class="ik ik-bar-chart-2"></i><span>Dashboard</span></a>
                </div>
                <div class="nav-item {{set_active('kasir.pasien')}}">
                    <a href="{{route('kasir.pasien')}}"><i class="ik ik-menu"></i><span>Daftar Pasien </span> </a>
                </div>                
            </nav>
        </div>
    </div>
</div>