<script src="{{url('assets/src/js/vendor/modernizr-2.8.3.min.js')}}"></script>

<script src="{{url('assets/src/js/vendor/jquery-3.3.1.min.js')}}"> </script>
<script src="{{url('assets/plugins/popper.js/dist/umd/popper.min.js')}}"></script>
<script src="{{url('assets/plugins/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{url('assets/plugins/perfect-scrollbar/dist/perfect-scrollbar.min.js')}}"></script>
<script src="{{url('assets/plugins/screenfull/dist/screenfull.js')}}"></script>
<script src="{{url('assets/plugins/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{url('assets/plugins/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{url('assets/plugins/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{url('assets/plugins/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')}}"></script>
<script src="{{url('assets/plugins/moment/moment.js')}}"></script>
<script src="{{url('assets/plugins/d3/dist/d3.min.js')}}"></script>
<script src="{{url('assets/plugins/c3/c3.min.js')}}"></script>
<script src="{{url('assets/dist/js/theme.min.js')}}"></script>