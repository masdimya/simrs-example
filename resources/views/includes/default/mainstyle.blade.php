<link rel="icon" href="{{url('favicon.ico')}}" type="image/x-icon" />

<link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,600,700,800" rel="stylesheet">

<link rel="stylesheet" href="{{url('assets/plugins/bootstrap/dist/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{url('assets/plugins/fontawesome-free/css/all.min.css')}}">
<link rel="stylesheet" href="{{url('assets/plugins/icon-kit/dist/css/iconkit.min.css')}}">
<link rel="stylesheet" href="{{url('assets/plugins/ionicons/dist/css/ionicons.min.css')}}">
<link rel="stylesheet" href="{{url('assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css')}}">
<link rel="stylesheet" href="{{url('assets/plugins/c3/c3.min.css')}}">
<link rel="stylesheet" href="{{url('assets/plugins/owl.carousel/dist/assets/owl.carousel.min.css')}}">
<link rel="stylesheet" href="{{url('assets/plugins/owl.carousel/dist/assets/owl.theme.default.min.css')}}">
<link rel="stylesheet" href="{{url('assets/dist/css/theme.css')}}">