<div class="app-sidebar colored">
    <div class="sidebar-header">
        <a class="header-brand" href="index.html">
            <div class="logo-img">
               <img src="src/img/brand-white.svg" class="header-brand-img" alt="lavalite"> 
            </div>
            <span class="text">ThemeKit</span>
        </a>
        <button type="button" class="nav-toggle"><i data-toggle="expanded" class="ik ik-toggle-right toggle-icon"></i></button>
        <button id="sidebarClose" class="nav-close"><i class="ik ik-x"></i></button>
    </div>
    
    <div class="sidebar-content">
        <div class="nav-container">
            <nav id="main-menu-navigation" class="navigation-main">
        
                <div class="nav-item {{set_active('poli-anak.dashboard')}}">
                    <a href="{{route('poli-anak.dashboard')}}"><i class="ik ik-bar-chart-2"></i><span>Dashboard</span></a>
                </div>
                <div class="nav-item {{set_active('poli-anak.antrian')}}">
                    <a href="{{route('poli-anak.antrian')}}"><i class="ik ik-menu"></i><span>Daftar Antrian </span> </a>
                </div>
                <div class="nav-item has-sub {{set_active(['poli-anak.pasien','poli-anak.diagnosa'])}}">
                    <a href="javascript:void(0)"><i class="ik ik-layers"></i><span>Pasien</span></a>
                    <div class="submenu-content">
                        <a href="{{route('poli-anak.pasien')}}" class="menu-item {{set_active('poli-anak.pasien')}}">Daftar Pasien</a> 
                        <a href="{{route('poli-anak.diagnosa')}}" class="menu-item {{set_active('poli-anak.diagnosa')}}">Tambah Hasil Diagnosa</a>
                    </div>
                </div>
                {{-- <div class="nav-item {{set_active('poli-anak.action')}}">
                    <a href="{{route('poli-anak.action')}}"><i class="ik ik-menu"></i><span>Pasien Poli Anak</span> </a>
                </div> --}}
                
            </nav>
        </div>
    </div>
</div>