<script src="{{url('assets/src/js/vendor/modernizr-2.8.3.min.js')}}"></script>

<script src="{{url('assets/src/js/vendor/jquery-3.3.1.min.js')}}"> </script>
<script src="{{url('assets/plugins/popper.js/dist/umd/popper.min.js')}}"></script>
<script src="{{url('assets/plugins/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{url('assets/plugins/perfect-scrollbar/dist/perfect-scrollbar.min.js')}}"></script>
<script src="{{url('assets/plugins/screenfull/dist/screenfull.js')}}"></script>
<script src="{{url('assets/plugins/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{url('assets/plugins/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{url('assets/plugins/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{url('assets/plugins/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')}}"></script>
<script src="{{url('assets/plugins/moment/moment.js')}}"></script>
<script src="{{url('assets/plugins/d3/dist/d3.min.js')}}"></script>
<script src="{{url('assets/plugins/c3/c3.min.js')}}"></script>
<script src="{{url('assets/dist/js/theme.min.js')}}"></script>

<script src="{{url('assets//js/datatables.js')}}"></script>
<script src="{{url('assets/plugins/select2/dist/js/select2.min.js')}}"></script>
<script src="{{url('assets/plugins/tempusdominus-bootstrap-4/build/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<script src="{{url('assets/plugins/jquery-minicolors/jquery.minicolors.min.js')}}"></script>
<script src="{{url('assets/plugins/datedropper/datedropper.min.js')}}"></script>
<script src="{{url('assets/js/form-picker.js')}}"></script>

{{-- hide div radio button registrasi --}}
<script>
    $(document).ready(function() {
        $("div.desc").hide();
        $("input[name$='registrasi']").click(function() {
            var test = $(this).val();
            $("div.desc").hide();
            $("#" + test).show();
        });
    });
</script>

{{-- hide div radio button pembayaran --}}
<script>
    $(document).ready(function() {
        $("div.desc-pembayaran").hide();
        $("input[name$='pembayaran']").click(function() {
            var test = $(this).val();
            $("div.desc-pembayaran").hide();
            $("#" + test).show();
        });
    });
</script>

{{-- hide div dropdown pendamping --}}
<script>
    $(document).ready(function(){
        $("select").change(function(){
            $(this).find("option:selected").each(function(){
                var optionValue = $(this).attr("value");
                if(optionValue){
                    $(".pendamping").not("." + optionValue).hide();
                    $("." + optionValue).show();
                } else{
                    $(".pendamping").hide();
                }
            });
        }).change();
    });
</script>

{{-- script datepicker --}}
<script>
    (function (b, o, i, l, e, r) {
            b.GoogleAnalyticsObject = l; b[l] || (b[l] =
                function () { (b[l].q = b[l].q || []).push(arguments) }); b[l].l = +new Date;
                e = o.createElement(i); r = o.getElementsByTagName(i)[0];
                e.src = 'https://www.google-analytics.com/analytics.js';
                r.parentNode.insertBefore(e, r)
            }(window, document, 'script', 'ga'));
        ga('create', 'UA-XXXXX-X', 'auto'); ga('send', 'pageview');
</script>