<div class="app-sidebar colored">
    <div class="sidebar-header">
        <a class="header-brand" href="index.html">
            <div class="logo-img">
                {{-- <img src="src/img/brand-white.svg" class="header-brand-img" alt="lavalite"> --}}
            </div>
            <span class="text">ThemeKit</span>
        </a>
        <button type="button" class="nav-toggle"><i data-toggle="expanded"
                class="ik ik-toggle-right toggle-icon"></i></button>
        <button id="sidebarClose" class="nav-close"><i class="ik ik-x"></i></button>
    </div>

    <div class="sidebar-content">
        <div class="nav-container">
            <nav id="main-menu-navigation" class="navigation-main">


                <div class="nav-lavel">Administrasi</div>
                <div class="nav-item {{set_active('registrasi.pasien')}}">
                    <a href="{{ route('registrasi.pasien') }}"><i class="ik ik-user-plus"></i><span>Registrasi
                            Pasien</span></a>
                </div>

                <div class="nav-item {{set_active('informasi.pasien')}}">
                    <a class="" href="{{ route('informasi.pasien') }}"><i class="ik ik-users"></i><span>Informasi
                            Pasien</span></a>
                </div>

            </nav>
        </div>
    </div>
</div>