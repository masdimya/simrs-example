@if (session()->has('message'))
<div class="alert alert-{{session('message.type')}} alert-dismissible fade show" role="alert">
    <strong> {{ session('message.body') }} </strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <i class="ik ik-x"></i>
    </button>
</div>
@endif

