@extends('layouts.kasir.main')

@section('title','Title Here')

@section('main-content')

<div class="main-content">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="ik ik-inbox bg-blue"></i>
                        <div class="d-inline">
                            <h5>Daftar Pasien</h5>

                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <nav class="breadcrumb-container" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="../index.html"><i class="ik ik-home"></i></a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">Pasien</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Daftar Pasien</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3>Daftar Pasien Poli Anak</h3>
                    </div>
                    <div class="card-body">
                        <table id="data_table" class="table">
                            <thead>
                                <tr>
                                    <th>No. RM</th>
                                    <th>Nama</th>
                                    <th>NIK</th>
                                    <th>Jenis Kelamin</th>
                                    <th>Alamat</th>
                                    <th>Nama Dokter</th>
                                    <th>Waktu Periksa</th>
                                    <th>Status</th>
                                    <th class="nosort">Aksi</th>

                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>001</td>
                                    <td>Erich Heaney</td>
                                    <td>12345678</td>
                                    <td>Laki-laki</td>
                                    <td>Bangkalan</td>
                                    <td>dr. Tantri Karimah Pudjiastuti</td>
                                    <td>16-02-2020 13:11</td>
                                    <td>
                                        @include('extras.badge',['label'=>'success','value'=>'Telah Diperiksa'])
                                    </td>
                                    <td>

                                        <div class="input-group-prepend">
                                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Opsi <i class="ik ik-chevron-down"></i></button>
                                            <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 35px, 0px);">
                                                <a class="dropdown-item " href="#!"><i class="ik ik-book-open"></i> <span>Detail</span></a>
                                                <a class="dropdown-item" href="#!"><i class="ik ik-edit-2"></i> <span>Ubah</span></a>
                                                <a class="dropdown-item" href="#!"><i class="ik ik-plus"></i> <span>Tambah Diagnosa</span></a>
                                                <div role="separator" class="dropdown-divider"></div>
                                                <a class="dropdown-item" href="#!"><i class="ik ik-trash"></i> <span>Hapus</span></a>
                                            </div>
                                        </div>
                                        
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalCenterLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterLabel">Pelayanan Poli Anak</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <h6> Detail Pasien </h6>
                        <div >
                            <table>
                                <tr>
                                    <td> No. RM </td>
                                    <td width="1%"> :&nbsp;&nbsp; </td>
                                    <td> 345678 </td>
                                </tr>
                                <tr>
                                    <td> Nama </td>
                                    <td width="1%"> :&nbsp;&nbsp; </td>
                                    <td> Erich Heaney </td>
                                </tr>
                                <tr>
                                    <td> Nama Orang Tua </td>
                                    <td width="1%"> :&nbsp;&nbsp; </td>
                                    <td> Budi</td>
                                </tr>
                                <tr>
                                    <td> Alamat </td>
                                    <td width="1%"> :&nbsp;&nbsp; </td>
                                    <td> Bangkalan</td>
                                </tr>
                            </table>

                            <div class="form-group mt-10" >
                                <h6 for="pilihdokter">Pilih Dokter</h6>
                                <select class="form-control" id="pilihdokter">
                                  <option>dr. Catur Anggriawan</option>
                                  <option>dr. Mahmud Maheswara</option>
                                  <option>dr. Kamidin Zulkarnain</option>
                                  <option>dr. Tantri Karimah Pudjiastuti </option>
                                  <option>dr. Elisa Indah Pratiwi</option>
                                </select>
                              </div>

                        </div>

                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="button" class="btn btn-primary">Layani</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="demoModal" tabindex="-1" role="dialog" aria-labelledby="demoModalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-sm " role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="demoModalLabel">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        ...
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>

@endsection
