@extends('layouts.poli.main-poli')

@section('title','Title Here')

@section('main-content')

<div class="main-content">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="ik ik-menu bg-blue"></i>
                        <div class="d-inline">
                            <h5>Navbar</h5>
                            <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <nav class="breadcrumb-container" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="../index.html"><i class="ik ik-home"></i></a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Navbar</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
        
        {{--  If use flash message, insert here --}}
        @include('extras.flash-message')
        
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3>Header</h3>
                    </div>
                    <div class="card-body">
                        <h4 class="sub-title">header-theme="light"</h4>
                        <p class="mb-10">Add <code>header-theme="light"</code> attribute in <code>.header-top</code> class</p>
                        <img src="../img/navbar/light.jpg" class="img-fluid border p-1 rounded" alt="">

                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection