@extends('layouts.poli.main-poli')

@section('title','Title Here')

@section('main-content')

<div class="main-content">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="ik ik-edit bg-red"></i>
                        <div class="d-inline">
                            <h5>Tambah Diagnosa <br> Pasien</h5>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <nav class="breadcrumb-container" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="../index.html"><i class="ik ik-home"></i></a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">Pasien</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Tambah Diagnosa</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><h3>Data Diri Pasien</h3></div>
                    <div class="card-body">
                        <form class="forms-sample">
                            <div class="form-group row">
                                <label for="no_pendaftaran" class="col-sm-3 col-form-label">No Pendaftaran</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="no_pendaftaran" name="no_pendaftaran" placeholder="No Pendaftaran">
                                </div>
                                <div class="col-sm-2">
                                    <button class="btn btn-primary">Cek Nomor</button>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="no_rm" class="col-sm-3 col-form-label">No. Rekam Medik</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="no_rm" name="no_rm" placeholder="Nomor Rekam Medik" >
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="nama" class="col-sm-3 col-form-label">Nama </label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Pasien" >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="umur" class="col-sm-3 col-form-label">Umur</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="umur" name="umur" placeholder="Umur Pasien" >
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h3>Resep Dokter</h3>
                    </div>
                    
                    <div class="card-body">
                        <div class="form-group row">
                            <div class="col-sm-2">
                                <button class="btn btn-primary">Tambah Resep</button>
                            </div>
                        </div>
                        <form class="forms-sample">
                            <div id="resep[]">  
                                <div class="title">
                                    <div class="form-group row mt-30">
                                        <div class="col-sm-9">
                                            <h4 class="sub-title ">Resep 1</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="content">
                                    <div class="form-group row">
                                        <label for="nama_obat" class="col-sm-3 col-form-label">Nama Obat</label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" id="nama_obat" name="nama_obat" placeholder="Nama Obat" >
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="jml_obat" class="col-sm-3 col-form-label">Jumlah </label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" id="jml_obat" name="jml_obat" placeholder="Jumlah Obat" >
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="dosis_obat" class="col-sm-3 col-form-label">Dosis </label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" id="dosis_obat" name="dosis_obat" placeholder="Dosis Obat" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id=resep[]>  
                                <div class="title ">
                                    <div class="form-group row mt-30">
                                        <div class="col-sm-9">
                                            <h4 class="sub-title ">Resep 2</h4>
                                        </div>
                                        <div class="col-sm-1">
                                            <button class="btn btn-danger ik ik-x" type="button"></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="content">
                                    <div class="form-group row">
                                        <label for="nama_obat" class="col-sm-3 col-form-label">Nama Obat</label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" id="nama_obat" name="nama_obat" placeholder="Nama Obat" >
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="jml_obat" class="col-sm-3 col-form-label">Jumlah </label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" id="jml_obat" name="jml_obat" placeholder="Jumlah Obat" >
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="dosis_obat" class="col-sm-3 col-form-label">Dosis </label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" id="dosis_obat" name="dosis_obat" placeholder="Dosis Obat" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header"><h3>Hasil Diagnosa</h3></div>
                    <div class="card-body">
                        <form class="forms-sample">
                            <div class="form-group row">
                                <div class="col-sm-2">
                                    <button class="btn btn-primary">Tambah Hasil Diagnosa</button>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-9">
                                    <div class="input-group input-group-button">
                                        <input type="text" class="form-control" placeholder="Hasil diagnosa 1">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-10">
                                    <div class="input-group input-group-button">
                                        <input type="text" class="form-control" placeholder="Hasil diagnosa 2">
                                        <div class="input-group-append">
                                            <button class="btn btn-danger ik ik-x" type="button"></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
            
        </div>

        <div class="row">
            <div class="col md-12">
                <button class="btn btn-secondary">Batal</button>
                <button class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>

@endsection