@extends('layouts.pasien.pasien-main')

@section('title','Registrasi Pasien')

@section('main-content')

<div class="main-content">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="ik ik-user-plus bg-blue"></i>
                        <div class="d-inline">
                            <h5 class="pt-2">Halaman @yield('title')</h5>
                            {{-- <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span> --}}
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <nav class="breadcrumb-container" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="../index.html"><i class="ik ik-home"></i></a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Registrasi Pasien</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>

        {{--  If use flash message, insert here --}}
        @include('extras.flash-message')


        <form action="">
        {{-- Identitas Pribadi --}}
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3>Identitas Pribadi</h3>
                    </div>
                    <div class="card-body">
                        <form class="forms-sample">
                            <div class="form-group row">
                                <label for="tipe_identitas" class="col-sm-3 col-form-label">Tipe Identitas</label>
                                <div class="form-radio pl-3">
                                    <div class="form-radio">
                                        <form>
                                            <div class="radio radio radiofill radio-inline">
                                                <label>
                                                    <input type="radio" name="radio" checked="checked">
                                                    <i class="helper"></i>KTP
                                                </label>
                                            </div>
                                            <div class="radio radio radiofill radio-inline">
                                                <label>
                                                    <input type="radio" name="radio">
                                                    <i class="helper"></i>SIM
                                                </label>
                                            </div>
                                            <div class="radio radio radiofill radio-inline">
                                                <label>
                                                    <input type="radio" name="radio">
                                                    <i class="helper"></i>PASPORT
                                                </label>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="no_identitas" class="col-sm-3 col-form-label">No. Identitas</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="no_identitas"
                                        placeholder="No. Identitas">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="nama_pasien" class="col-sm-3 col-form-label">Nama Pasien <span
                                        style="color:red">*</span></label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="nama_pasien" placeholder="Nama Pasien">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="jenis_kelamin" class="col-sm-3 col-form-label">Jenis Kelamin <span
                                        style="color:red">*</span></label>
                                <div class="form-radio pl-3">
                                    <div class="form-radio">
                                        <form>
                                            <div class="radio radio radiofill radio-inline">
                                                <label>
                                                    <input type="radio" name="radio" checked="checked">
                                                    <i class="helper"></i>Laki-laki
                                                </label>
                                            </div>
                                            <div class="radio radio radiofill radio-inline">
                                                <label>
                                                    <input type="radio" name="radio">
                                                    <i class="helper"></i>Perempuan
                                                </label>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="tgl_lahir" class="col-sm-3 col-form-label">Tanggal Lahir <span
                                        style="color:red">*</span></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control datetimepicker-input" id="datepicker"
                                        data-toggle="datetimepicker" data-target="#datepicker"
                                        placeholder="Pilih Tgl. Lahir">
                                </div>


                                <label for="umur" class="col-sm-1 col-form-label">Umur</label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control" id="umur" placeholder="Umur" readonly>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="no_tlpn" class="col-sm-3 col-form-label">No. Tlpn <span
                                        style="color:red">*</span></label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="no_tlpn" placeholder="No. Tlpn">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-sm-3 col-form-label">Email</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="email" placeholder="Email">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="pekerjaan" class="col-sm-3 col-form-label">Pekerjaan <span
                                        style="color:red">*</span></label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="pekerjaan" placeholder="Pekerjaan">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="jenis_kelamin" class="col-sm-3 col-form-label">Status Pernikahan <span
                                        style="color:red">*</span></label>
                                <div class="form-radio pl-3">
                                    <div class="form-radio">
                                        <form>
                                            <div class="radio radio radiofill radio-inline">
                                                <label>
                                                    <input type="radio" name="radio" checked="checked">
                                                    <i class="helper"></i>Belum Kawin
                                                </label>
                                            </div>
                                            <div class="radio radio radiofill radio-inline">
                                                <label>
                                                    <input type="radio" name="radio">
                                                    <i class="helper"></i>Kawin
                                                </label>
                                            </div>
                                            <div class="radio radio radiofill radio-inline">
                                                <label>
                                                    <input type="radio" name="radio">
                                                    <i class="helper"></i>Duda
                                                </label>
                                            </div>
                                            <div class="radio radio radiofill radio-inline">
                                                <label>
                                                    <input type="radio" name="radio">
                                                    <i class="helper"></i>Janda
                                                </label>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="agama" class="col-sm-3 col-form-label">Agama <span
                                        style="color:red">*</span></label>
                                <div class="col-md-9">
                                    <select class="form-control" id="agama">
                                        <option value="islam">Islam</option>
                                        <option value="kristen-protestan">Kristen Protestan</option>
                                        <option value="katolik">Katolik</option>
                                        <option value="hindu">Hindu</option>
                                        <option value="buddha">Buddha</option>
                                        <option value="kong-hu-cu">Kong Hu Cu</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="suku" class="col-sm-3 col-form-label">Suku <span
                                        style="color:red">*</span></label>
                                <div class="col-md-9">
                                    <select class="form-control" id="suku">
                                        <option value="jawa">Jawa</option>
                                        <option value="sunda">Sunda</option>
                                        <option value="batak">Batak</option>
                                        <option value="madura">Madura</option>
                                        <option value="betawi">Betawi</option>
                                        <option value="minangkabau">Minangkabau</option>
                                        <option value="bugis">Bugis</option>
                                        <option value="melayu">Melayu</option>
                                        <option value="arab">Arab</option>
                                        <option value="banten">Banten</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="nama-ibu-kandung" class="col-sm-3 col-form-label">Nama Ibu Kandung</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" id="nama-ibu-kandung"
                                        placeholder="Nama Ibu Kandung">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        {{-- Identitas Pribadi --}}

        {{-- Alamat Pasien --}}
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3>Alamat Pasien</h3>
                    </div>
                    <div class="card-body">
                        <form class="forms-sample">
                            <div class="form-group row">
                                <label for="alamat" class="col-sm-3 col-form-label">Alamat sesuai identitas <span
                                        style="color:red">*</span></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" id="alamat" rows="4"></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="negara" class="col-sm-3 col-form-label">Negara <span
                                        style="color:red">*</span></label>
                                <div class="col-md-9">
                                    <select class="form-control" id="negara">
                                        <option value="">--Pilih Negara--</option>
                                        <option value="indonesia">Indonesia</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="provinsi" class="col-sm-3 col-form-label">Provinsi <span
                                        style="color:red">*</span></label>
                                <div class="col-md-9">
                                    <select class="form-control" id="provinsi">
                                        <option value="">--Pilih Provinsi--</option>
                                        <option value="jawa-timur">Jawa Timur</option>
                                        <option value="jawa-barat">Jawa Barat</option>
                                        <option value="jawa-tengah">Jawa Tengah</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="kabupaten" class="col-sm-3 col-form-label">Kabupaten <span
                                        style="color:red">*</span></label>
                                <div class="col-md-9">
                                    <select class="form-control" id="kabupaten">
                                        <option value="">--Pilih Kabupaten--</option>
                                        <option value="bangkalan">Kab. Bangkalan</option>
                                        <option value="blitar">Kab. Blitar</option>
                                        <option value="kediri">Kab. Kediri</option>
                                        <option value="malang">Kab. Malang</option>
                                        <option value="jember">Kab. Jember</option>
                                        <option value="lumajang">Kab. Lumajang</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="kecamatan" class="col-sm-3 col-form-label">Kecamatan <span
                                        style="color:red">*</span></label>
                                <div class="col-md-9">
                                    <select class="form-control" id="kecamatan">
                                        <option value="">--Pilih Kecamatan--</option>
                                        <option value="kamal">Bangkalan</option>
                                        <option value="kamal">Kamal</option>
                                        <option value="labang">Labang</option>
                                        <option value="galis">Galis</option>
                                        <option value="modung">Modung</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="kelurahan" class="col-sm-3 col-form-label">Kelurahan</label>
                                <div class="col-md-9">
                                    <select class="form-control" id="kelurahan">
                                        <option value="">--Pilih Kelurahan--</option>
                                        <option value="ujung-piring">Ujung Piring</option>
                                        <option value="sembilangan">Sembilangan</option>
                                        <option value="kramat">Kramat</option>
                                        <option value="martajesah">Martajesah</option>
                                        <option value="mlajej">Mlajah</option>
                                        <option value="kemayoran">Kemayoran</option>
                                        <option value="pengeranan">Pangeranan</option>
                                        <option value="demangan">Demangan</option>
                                        <option value="kraton">Kraton</option>
                                        <option value="pejagan">Pejagan</option>
                                        <option value="bancaran">Bancaran</option>
                                        <option value="sabiyan">Sabiyan</option>
                                        <option value="gebang">Gebang</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="rt" class="col-sm-3 col-form-label">RT</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="rt" placeholder="RT">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="rw" class="col-sm-3 col-form-label">RW</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="rw" placeholder="RW">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        {{-- Jenis Registrasi --}}
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3>Jenis Registrasi</h3>
                    </div>
                    <div class="card-body">
                        <form class="forms-sample">
                            <div class="form-group row">
                                <label for="jenis-perawatan" class="col-sm-3 col-form-label">Jenis Perawatan <span
                                        style="color:red">*</span></label>
                                <div class="form-radio pl-3 pt-1">
                                    <div class="form-radio">
                                        <form>
                                            <div class="radio radio radiofill radio-inline">
                                                <label>
                                                    <input type="radio" name="registrasi" value="igd">
                                                    <i class="helper"></i>IGD
                                                </label>
                                            </div>
                                            <div class="radio radio radiofill radio-inline">
                                                <label>
                                                    <input type="radio" name="registrasi" value="irja">
                                                    <i class="helper"></i>IRJA
                                                </label>
                                            </div>
                                            <div class="radio radio radiofill radio-inline">
                                                <label>
                                                    <input type="radio" name="registrasi" value="irna">
                                                    <i class="helper"></i>IRNA
                                                </label>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        {{-- pembayaran --}}
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3>Pembayaran Pasien</h3>
                    </div>
                    <div class="card-body">
                        <form class="forms-sample">
                            <div class="form-group row">
                                <label for="jenis-perawatan" class="col-sm-3 col-form-label">Tipe Pembayaran <span
                                        style="color:red">*</span></label>
                                <div class="form-radio pl-3 pt-1">
                                    <div class="form-radio">

                                        <div class="radio radio radiofill radio-inline">
                                            <label>
                                                <input type="radio" name="pembayaran" value="tunai">
                                                <i class="helper"></i>Tunai
                                            </label>
                                        </div>
                                        <div class="radio radio radiofill radio-inline">
                                            <label>
                                                <input type="radio" name="pembayaran" value="jaminan">
                                                <i class="helper"></i>Jaminan (BPJS)
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="jaminan desc-pembayaran" id="jaminan">
                                <div class="form-group row">
                                    <label for="cari-bpjs" class="col-sm-3 col-form-label">Cari BPJS</label>
                                    <div class="col-sm-9 col-lg-9">
                                        <input type="button" class="btn btn-primary" id="bpjs" value="Cari BPJS">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="tgl_perawatan" class="col-sm-3 col-form-label">Tanggal Perawatan</label>
                                    <div class="col-sm-9 col-lg-9">
                                        <input type="text" class="form-control datetimepicker-input" id="datepicker2"
                                            data-toggle="datetimepicker" data-target="#datepicker2"
                                            placeholder="Pilih Tgl. Perawatan">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="no-sep" class="col-sm-3 col-form-label">No. SEP</label>
                                    <div class="col-sm-9 col-lg-9">
                                        <input type="text" class="form-control" id="no-sep" placeholder="No. SEP">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        {{-- igd --}}
        <div class="row">
            <div class="col-md-12 desc" id="igd">
                <div class="card">
                    <div class="card-header">
                        <h3>Upload Foto</h3>
                    </div>
                    <div class="card-body">
                        <form class="forms-sample">

                            <div class="form-group row">
                                <label for="upload-file" class="col-sm-3 col-form-label">File upload</label>
                                <div class="col-md-6">
                                    <input type="file" name="img[]" id="upload-file">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        {{-- irja --}}
        <div class="row">
            <div class="col-md-12 desc" id="irja">
                <div class="card">
                    <div class="card-header">
                        <h3>Klinik</h3>
                    </div>
                    <div class="card-body">
                        <form class="forms-sample">

                            <div class="form-group row">
                                <label for="klinik" class="col-sm-3 col-form-label">Klinik</label>
                                <div class="col-sm-9 col-lg-9">
                                    <select name="klinik" class="form-control" id="klinik">
                                        <option value="">--Pilih Klinik Tujuan--</option>
                                        <option value="">Anak - ANA</option>
                                        <option value="">Bedah Anak - BDA</option>
                                        <option value="">Bedah Plastik - BDP</option>
                                        <option value="">Bedah Syaraf - BSY</option>
                                        <option value="">Bedah Toraks Kardiovaskuler (Bedah TKV) - BTK</option>
                                        <option value="">Bedah Tulang - ORT</option>
                                        <option value="">Bedah Tumor - ONK</option>
                                        <option value="">Bedah Umum - BED</option>
                                        <option value="">Bedah Urologi - URO</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="prioritas" class="col-sm-3 col-form-label">Prioritas</label>
                                <div class="col-sm-9 col-lg-9">
                                    <select name="prioritas" class="form-control" id="prioritas">
                                        <option value="">--Pilih Prioritas--</option>
                                        <option value="">Utama</option>
                                        <option value="">Tidak Utama</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header">
                        <h3>Pendamping Pasien</h3>
                    </div>
                    <div class="card-body">
                        <form class="forms-sample">

                            <div class="form-group row">
                                <label for="pendamping" class="col-sm-3 col-form-label">Status pendamping</label>
                                <div class="col-sm-9 col-lg-9">
                                    <select class="form-control" id="pendamping">
                                        <option value="">Tanpa Pendamping</option>
                                        <option value="pendamping">Dengan Pendamping</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row pendamping">
                                <label for="nama-pendamping" class="col-sm-3 col-form-label">Nama pendamping</label>
                                <div class="col-sm-9 col-lg-9">
                                    <input type="text" class="form-control" name="nama-pendamping" id="nama-pendamping"
                                        placeholder="Nama Pendamping">
                                </div>
                            </div>

                            <div class="form-group row pendamping">
                                <label for="alamat-pendamping" class="col-sm-3 col-form-label">Alamat pendamping</label>
                                <div class="col-sm-9 col-lg-9">
                                    <textarea class="form-control" name="alamat-pendamping" id="alamat-pendamping"
                                        rows="4" placeholder="Alamat Pendamping"></textarea>
                                </div>
                            </div>

                            <div class="form-group row pendamping">
                                <label for="notlpn-pendamping" class="col-sm-3 col-form-label">No. Tlpn
                                    pendamping</label>
                                <div class="col-sm-9 col-lg-9">
                                    <input type="text" class="form-control" name="notlpn-pendamping"
                                        id="notlpn-pendamping" placeholder="No. Tlpn Milik Pendamping">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        {{-- irna --}}
        <div class="row">
            <div class="col-md-12 desc" id="irna">
                <div class="card">
                    <div class="card-header">
                        <h3>Diagnosis</h3>
                    </div>
                    <div class="card-body">
                        <form class="forms-sample">
                            <div class="form-group row">
                                <label for="diagnosis-utama" class="col-sm-3 col-form-label">Diagnosis Utama <span
                                        style="color:red">*</span></label>
                                <div class="col-sm-9 col-lg-9">
                                    <select name="diagnosis-utama" class="form-control select2 " id="diagnosis-utama">
                                        <option value="">--Pilih Diagnosis Utama--</option>
                                        <option value="">A39.5 - Meningococcal Heart Disease</option>
                                        <option value="">B57.0 - Acute Chagas' Disease With Heart Involvement</option>
                                        <option value="">B57.1 - Acute Chagas' Disease Without Heart Involvement
                                        </option>
                                        <option value="">B57.2 - Chagas' Disease (Chronic) With Heart Involvement
                                        </option>
                                        <option value="">C38 - Malignant Neoplasm of Heart</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="diagnosis-sekunder" class="col-sm-3 col-form-label">Diagnosis
                                    Sekunder</label>
                                <div class="col-sm-9 col-lg-9">
                                    <select name="diagnosis-sekunder" class="form-control" id="diagnosis-sekunder">
                                        <option value="">--Pilih Diagnosis Sekunder--</option>
                                        <option value="">A39.5 - Meningococcal Heart Disease</option>
                                        <option value="">B57.0 - Acute Chagas' Disease With Heart Involvement</option>
                                        <option value="">B57.1 - Acute Chagas' Disease Without Heart Involvement
                                        </option>
                                        <option value="">B57.2 - Chagas' Disease (Chronic) With Heart Involvement
                                        </option>
                                        <option value="">C38 - Malignant Neoplasm of Heart</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header">
                        <h3>Dokter (DPJP)</h3>
                    </div>
                    <div class="card-body">
                        <form class="forms-sample">
                            <div class="form-group row">
                                <label for="dokter-bpjp" class="col-sm-3 col-form-label">Dokter <span
                                        style="color:red">*</span></label>
                                <div class="col-sm-9 col-lg-9">
                                    <select name="dokter-bpjp" class="form-control" id="dokter-bpjp">
                                        <option value="">--Pilih Dokter DPJP--</option>
                                        <option value="">Hermawati - Bedah Umum</option>
                                        <option value="">Muhammad Albar - Perawatan Luka 1</option>
                                        <option value="">Muhammad Iqbal - Kandungan & Kebidanan, Gigi dan Mulut</option>
                                        <option value="">Sri Ariyani - Anak, Bedah Umum</option>
                                        <option value="">Yanti Holijah - Bedah Tumor</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header">
                        <h3>Ruangan</h3>
                    </div>
                    <div class="card-body">
                        <form class="forms-sample">
                            <div class="form-group row">
                                <label for="ruangan" class="col-sm-3 col-form-label">Ruangan <span
                                        style="color:red">*</span></label>
                                <div class="col-sm-9 col-lg-9">
                                    <select name="ruangan" class="form-control" id="ruangan">
                                        <option value="">--Pilih Ruangan--</option>
                                        <option value="">Anggrek (Non Intensive) | Kamar : 22 | Tempat Tidur Tersedia :
                                            56
                                        </option>
                                        <option value="">Angsoka (Non Intensive) | Kamar : 11 | Tempat Tidur Tersedia :
                                            50
                                        </option>
                                        <option value="">Aster (Non Intensive) | Kamar : 10 | Termpat Tidur Tersedia :
                                            53
                                        </option>
                                        <option value="">Bayi / Lily (Non Intensive) | Kamar : 1 | Tempat Tidur Tersedia
                                            :
                                            29</option>
                                        <option value="">Bayi Resusitasi (Intensive) | Kamar : 1 | Tempat Tidur Tersedua
                                            : 4
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="kelas" class="col-sm-3 col-form-label">Kelas <span
                                        style="color:red">*</span></label>
                                <div class="col-sm-9 col-lg-9">
                                    <select name="kelas" class="form-control" id="kelas">
                                        <option value="">--Pilih Kelas--</option>
                                        <option value="">Kelas 1</option>
                                        <option value="">Kelas 2</option>
                                        <option value="">Kelas 3</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header">
                        <h3>Pendamping Pasien</h3>
                    </div>
                    <div class="card-body">
                        <form class="forms-sample">

                            <div class="form-group row">
                                <label for="pendamping" class="col-sm-3 col-form-label">Status pendamping</label>
                                <div class="col-sm-9 col-lg-9">
                                    <select class="form-control" id="pendamping">
                                        <option value="">Tanpa Pendamping</option>
                                        <option value="pendamping">Dengan Pendamping</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row pendamping">
                                <label for="nama-pendamping" class="col-sm-3 col-form-label">Nama pendamping</label>
                                <div class="col-sm-9 col-lg-9">
                                    <input type="text" class="form-control" name="nama-pendamping" id="nama-pendamping"
                                        placeholder="Nama Pendamping">
                                </div>
                            </div>

                            <div class="form-group row pendamping">
                                <label for="alamat-pendamping" class="col-sm-3 col-form-label">Alamat pendamping</label>
                                <div class="col-sm-9 col-lg-9">
                                    <textarea class="form-control" name="alamat-pendamping" id="alamat-pendamping"
                                        rows="4" placeholder="Alamat Pendamping"></textarea>
                                </div>
                            </div>

                            <div class="form-group row pendamping">
                                <label for="notlpn-pendamping" class="col-sm-3 col-form-label">No. Tlpn
                                    pendamping</label>
                                <div class="col-sm-9 col-lg-9">
                                    <input type="text" class="form-control" name="notlpn-pendamping"
                                        id="notlpn-pendamping" placeholder="No. Tlpn Milik Pendamping">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <button type="submit" class="btn btn-primary col-md-12">Simpan</button>
            </div>
        </div>

    </div>
</div>

<style>
    .datecard {
        background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 80 40' width='80' height='40'%3E%3Cpath fill='%236b6b6c' fill-opacity='0.16' d='M0 40a19.96 19.96 0 0 1 5.9-14.11 20.17 20.17 0 0 1 19.44-5.2A20 20 0 0 1 20.2 40H0zM65.32.75A20.02 20.02 0 0 1 40.8 25.26 20.02 20.02 0 0 1 65.32.76zM.07 0h20.1l-.08.07A20.02 20.02 0 0 1 .75 5.25 20.08 20.08 0 0 1 .07 0zm1.94 40h2.53l4.26-4.24v-9.78A17.96 17.96 0 0 0 2 40zm5.38 0h9.8a17.98 17.98 0 0 0 6.67-16.42L7.4 40zm3.43-15.42v9.17l11.62-11.59c-3.97-.5-8.08.3-11.62 2.42zm32.86-.78A18 18 0 0 0 63.85 3.63L43.68 23.8zm7.2-19.17v9.15L62.43 2.22c-3.96-.5-8.05.3-11.57 2.4zm-3.49 2.72c-4.1 4.1-5.81 9.69-5.13 15.03l6.61-6.6V6.02c-.51.41-1 .85-1.48 1.33zM17.18 0H7.42L3.64 3.78A18 18 0 0 0 17.18 0zM2.08 0c-.01.8.04 1.58.14 2.37L4.59 0H2.07z'%3E%3C/path%3E%3C/svg%3E");

    }
</style>

@endsection


{{-- <script src="{{url('assets/src/js/vendor/jquery-3.3.1.min.js')}}"> </script> --}}