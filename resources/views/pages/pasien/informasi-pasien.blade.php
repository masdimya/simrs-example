@extends('layouts.pasien.pasien-main')

@section('title','Informasi Pasien')

@section('main-content')

<div class="main-content">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="ik ik-users bg-blue"></i>
                        <div class="d-inline">
                            <h5 class="pt-2">Halaman @yield('title')</h5>
                            {{-- <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span> --}}
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <nav class="breadcrumb-container" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="../index.html"><i class="ik ik-home"></i></a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Informasi Pasien</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>

        {{--  If use flash message, insert here --}}
        @include('extras.flash-message')

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3>Informasi Pasien</h3>
                    </div>
                    <div class="card-body">
                        <table id="data_table" class="table ">
                            <thead>
                                <tr>
                                    <th>NIK</th>
                                    <th>Nama</th>
                                    <th>No. RM</th>
                                    <th>Jenis Kelamin</th>
                                    <th>Tanggal Lahir</th>
                                    <th>Alamat</th>
                                    <th class="nosort">Status Pasien</th>
                                    <th>Registrasi Terakhir</th>
                                    <th class="nosort">Option</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>123456789123</td>
                                    <td>Budianto</td>
                                    <td>01.00.01.88</td>
                                    <td>Laki-laki</td>
                                    <td>04-04-1994</td>
                                    <td>Jl. Durian RT3 RW5</td>
                                    <td><span class="badge badge-success mb-1">IGD</span></td>
                                    <td>15:26 - 14 February 2020</td>
                                    <td>
                                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#"
                                            role="button" aria-haspopup="true" aria-expanded="false">Pilihan<i
                                                class="ik ik-chevron-down"></i></a>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="#">Detail</a>
                                            <a class="dropdown-item" href="#">Edit</a>
                                            <a class="dropdown-item" href="{{ route('cetak.pasien') }}" target="_blank">Cetak Kartu RM</a>
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="#">Lainnya</a>
                                        </div>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>123456789123</td>
                                    <td>Budianto</td>
                                    <td>01.00.01.88</td>
                                    <td>Laki-laki</td>
                                    <td>04-04-1994</td>
                                    <td>Jl. Durian RT3 RW5</td>
                                    <td><span class="badge badge-success mb-1">IGD</span></td>
                                    <td>15:26 - 14 February 2020</td>
                                    <td>
                                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#"
                                            role="button" aria-haspopup="true" aria-expanded="false">Pilihan <i
                                                class="ik ik-chevron-down"></i></a>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="#">Detail</a>
                                            <a class="dropdown-item" href="#">Edit</a>
                                            <a class="dropdown-item" href="{{ route('cetak.pasien') }}" target="_blank">Cetak Kartu RM</a>
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="#">Lainnya</a>
                                        </div>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>123456789123</td>
                                    <td>Budianto</td>
                                    <td>01.00.01.88</td>
                                    <td>Laki-laki</td>
                                    <td>04-04-1994</td>
                                    <td>Jl. Durian RT3 RW5</td>
                                    <td><span class="badge badge-success mb-1">IGD</span></td>
                                    <td>15:26 - 14 February 2020</td>
                                    <td>
                                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#"
                                            role="button" aria-haspopup="true" aria-expanded="false">Pilihan <i
                                                class="ik ik-chevron-down"></i></a>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="#">Detail</a>
                                            <a class="dropdown-item" href="#">Edit</a>
                                            <a class="dropdown-item" href="{{ route('cetak.pasien') }}" target="_blank">Cetak Kartu RM</a>
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="#">Lainnya</a>
                                        </div>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>123456789123</td>
                                    <td>Budianto</td>
                                    <td>01.00.01.88</td>
                                    <td>Laki-laki</td>
                                    <td>04-04-1994</td>
                                    <td>Jl. Durian RT3 RW5</td>
                                    <td><span class="badge badge-success mb-1">IGD</span></td>
                                    <td>15:26 - 14 February 2020</td>
                                    <td>
                                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#"
                                            role="button" aria-haspopup="true" aria-expanded="false">Pilihan <i
                                                class="ik ik-chevron-down"></i></a>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="#">Detail</a>
                                            <a class="dropdown-item" href="#">Edit</a>
                                            <a class="dropdown-item" href="{{ route('cetak.pasien') }}" target="_blank">Cetak Kartu RM</a>
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="#">Lainnya</a>
                                        </div>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>123456789123</td>
                                    <td>Budianto</td>
                                    <td>01.00.01.88</td>
                                    <td>Laki-laki</td>
                                    <td>04-04-1994</td>
                                    <td>Jl. Durian RT3 RW5</td>
                                    <td><span class="badge badge-success mb-1">IGD</span></td>
                                    <td>15:26 - 14 February 2020</td>
                                    <td>
                                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#"
                                            role="button" aria-haspopup="true" aria-expanded="false">Pilihan <i
                                                class="ik ik-chevron-down"></i></a>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="#">Detail</a>
                                            <a class="dropdown-item" href="#">Edit</a>
                                            <a class="dropdown-item" href="{{ route('cetak.pasien') }}" target="_blank">Cetak Kartu RM</a>
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="#">Lainnya</a>
                                        </div>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>123456789123</td>
                                    <td>Budianto</td>
                                    <td>01.00.01.88</td>
                                    <td>Laki-laki</td>
                                    <td>04-04-1994</td>
                                    <td>Jl. Durian RT3 RW5</td>
                                    <td><span class="badge badge-success mb-1">IGD</span></td>
                                    <td>15:26 - 14 February 2020</td>
                                    <td>
                                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#"
                                            role="button" aria-haspopup="true" aria-expanded="false">Pilihan <i
                                                class="ik ik-chevron-down"></i></a>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="#">Detail</a>
                                            <a class="dropdown-item" href="#">Edit</a>
                                            <a class="dropdown-item" href="{{ route('cetak.pasien') }}" target="_blank">Cetak Kartu RM</a>
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="#">Lainnya</a>
                                        </div>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>123456789123</td>
                                    <td>Budianto</td>
                                    <td>01.00.01.88</td>
                                    <td>Laki-laki</td>
                                    <td>04-04-1994</td>
                                    <td>Jl. Durian RT3 RW5</td>
                                    <td><span class="badge badge-success mb-1">IGD</span></td>
                                    <td>15:26 - 14 February 2020</td>
                                    <td>
                                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#"
                                            role="button" aria-haspopup="true" aria-expanded="false">Pilihan <i
                                                class="ik ik-chevron-down"></i></a>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="#">Detail</a>
                                            <a class="dropdown-item" href="#">Edit</a>
                                            <a class="dropdown-item" href="{{ route('cetak.pasien') }}" target="_blank">Cetak Kartu RM</a>
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="#">Lainnya</a>
                                        </div>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>123456789123</td>
                                    <td>Budianto</td>
                                    <td>01.00.01.88</td>
                                    <td>Laki-laki</td>
                                    <td>04-04-1994</td>
                                    <td>Jl. Durian RT3 RW5</td>
                                    <td><span class="badge badge-success mb-1">IGD</span></td>
                                    <td>15:26 - 14 February 2020</td>
                                    <td>
                                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#"
                                            role="button" aria-haspopup="true" aria-expanded="false">Pilihan <i
                                                class="ik ik-chevron-down"></i></a>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="#">Detail</a>
                                            <a class="dropdown-item" href="#">Edit</a>
                                            <a class="dropdown-item" href="{{ route('cetak.pasien') }}" target="_blank">Cetak Kartu RM</a>
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="#">Lainnya</a>
                                        </div>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>123456789123</td>
                                    <td>Budianto</td>
                                    <td>01.00.01.88</td>
                                    <td>Laki-laki</td>
                                    <td>04-04-1994</td>
                                    <td>Jl. Durian RT3 RW5</td>
                                    <td><span class="badge badge-success mb-1">IGD</span></td>
                                    <td>15:26 - 14 February 2020</td>
                                    <td>
                                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#"
                                            role="button" aria-haspopup="true" aria-expanded="false">Pilihan <i
                                                class="ik ik-chevron-down"></i></a>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="#">Detail</a>
                                            <a class="dropdown-item" href="#">Edit</a>
                                            <a class="dropdown-item" href="{{ route('cetak.pasien') }}" target="_blank">Cetak Kartu RM</a>
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="#">Lainnya</a>
                                        </div>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>123456789123</td>
                                    <td>Budianto</td>
                                    <td>01.00.01.88</td>
                                    <td>Laki-laki</td>
                                    <td>04-04-1994</td>
                                    <td>Jl. Durian RT3 RW5</td>
                                    <td><span class="badge badge-success mb-1">IGD</span></td>
                                    <td>15:26 - 14 February 2020</td>
                                    <td>
                                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#"
                                            role="button" aria-haspopup="true" aria-expanded="false">Pilihan <i
                                                class="ik ik-chevron-down"></i></a>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="#">Detail</a>
                                            <a class="dropdown-item" href="#">Edit</a>
                                            <a class="dropdown-item" href="{{ route('cetak.pasien') }}" target="_blank">Cetak Kartu RM</a>
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="#">Lainnya</a>
                                        </div>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>123456789123</td>
                                    <td>Budianto</td>
                                    <td>01.00.01.88</td>
                                    <td>Laki-laki</td>
                                    <td>04-04-1994</td>
                                    <td>Jl. Durian RT3 RW5</td>
                                    <td><span class="badge badge-success mb-1">IGD</span></td>
                                    <td>15:26 - 14 February 2020</td>
                                    <td>
                                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#"
                                            role="button" aria-haspopup="true" aria-expanded="false">Pilihan <i
                                                class="ik ik-chevron-down"></i></a>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="#">Detail</a>
                                            <a class="dropdown-item" href="#">Edit</a>
                                            <a class="dropdown-item" href="{{ route('cetak.pasien') }}" target="_blank">Cetak Kartu RM</a>
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="#">Lainnya</a>
                                        </div>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>123456789123</td>
                                    <td>Budianto</td>
                                    <td>01.00.01.88</td>
                                    <td>Laki-laki</td>
                                    <td>04-04-1994</td>
                                    <td>Jl. Durian RT3 RW5</td>
                                    <td><span class="badge badge-success mb-1">IGD</span></td>
                                    <td>15:26 - 14 February 2020</td>
                                    <td>
                                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#"
                                            role="button" aria-haspopup="true" aria-expanded="false">Pilihan <i
                                                class="ik ik-chevron-down"></i></a>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="#">Detail</a>
                                            <a class="dropdown-item" href="#">Edit</a>
                                            <a class="dropdown-item" href="{{ route('cetak.pasien') }}" target="_blank">Cetak Kartu RM</a>
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="#">Lainnya</a>
                                        </div>
                                    </td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection